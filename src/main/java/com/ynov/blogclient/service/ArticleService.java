package com.ynov.blogclient.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ynov.blogclient.model.Article;
import com.ynov.blogclient.repository.ArticleProxy;

@Service
public class ArticleService {

	@Autowired
	private ArticleProxy articleProxy;
	
	public List<Article> getArticles() {
		return articleProxy.getArticles();
	}

	public Article getArticleById(Integer id) {
		return articleProxy.getArticleById(id);
	}

	public void save(Article article) {
		articleProxy.save(article);		
	}
	
}
