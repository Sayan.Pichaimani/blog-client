package com.ynov.blogclient.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ynov.blogclient.TokenContext;
import com.ynov.blogclient.model.ApiUser;
import com.ynov.blogclient.repository.LoginProxy;

@Service
public class LoginService {

	@Autowired
	private TokenContext tokenContext;
	
	@Autowired
	private LoginProxy loginProxy;
	
	public void login(ApiUser user) {
		String token = loginProxy.login(user);
		tokenContext.setToken(token);
	}
	
}
