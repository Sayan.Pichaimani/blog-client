package com.ynov.blogclient.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ynov.blogclient.model.Category;
import com.ynov.blogclient.repository.CategoryProxy;
import com.ynov.blogclient.repository.HomeProxy;

@Service
public class HomeService {
	
	@Autowired
	private HomeProxy homeProxy;
	
	public List<Category> getCategories() {
		return homeProxy.getCategories();
	}
	
	public Category getCategoryById(Integer id) {
		return homeProxy.getCategoryById(id);
	}
	
}
