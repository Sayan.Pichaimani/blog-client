package com.ynov.blogclient.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ynov.blogclient.model.Article;
import com.ynov.blogclient.service.ArticleService;

@Controller
@RequestMapping("public")
public class ArticleController {

	@Autowired
	private ArticleService articleService;
	
	@GetMapping("/articles")
	public String articlesPage(Model model) {
		List<Article> articles = articleService.getArticles();
		model.addAttribute("articles", articles);
		return "articles";
	}
	
	@GetMapping("/article/{id}")
	public String articlePage(@PathVariable(name = "id") Integer id, Model model) {
		Article article = articleService.getArticleById(id);
		model.addAttribute("article", article);
		return "article";
	}
}
