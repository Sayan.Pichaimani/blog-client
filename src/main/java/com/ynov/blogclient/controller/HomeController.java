package com.ynov.blogclient.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.ynov.blogclient.model.Category;
import com.ynov.blogclient.service.HomeService;

@Controller
public class HomeController {
	
	@Autowired
	private HomeService homeService;

	@GetMapping("/")
	public String getHomePage(Model model) {
		List<Category> categories = homeService.getCategories();
		model.addAttribute("categories", categories);
		
		return "home";
	}
}
