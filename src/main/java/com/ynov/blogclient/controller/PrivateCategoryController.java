package com.ynov.blogclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ynov.blogclient.model.Category;
import com.ynov.blogclient.service.CategoryService;

@Controller
@RequestMapping("private")
public class PrivateCategoryController {
	
	@Autowired
	private CategoryService categoryService;
	
	@PostMapping("/category")
	public ModelAndView createNewCategory(@ModelAttribute Category category) {
		categoryService.save(category);
		return new ModelAndView("redirect:/categories");
	}
	
	@GetMapping("/newCategory")
	public String newCategoryPage(Model model) {
		model.addAttribute("category", new Category());
		return "newCategory";
	}

}
