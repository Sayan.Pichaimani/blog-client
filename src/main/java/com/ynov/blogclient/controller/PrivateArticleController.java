package com.ynov.blogclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ynov.blogclient.model.Article;
import com.ynov.blogclient.service.ArticleService;

@Controller
@RequestMapping("private")
public class PrivateArticleController {
	
	@Autowired
	private ArticleService articleService;

	@PostMapping("/article")
	public ModelAndView createNewArticle(@ModelAttribute Article article) {
		articleService.save(article);
		return new ModelAndView("redirect:/articles");
	}
	
	@GetMapping("/newArticle")
	public String newArticlePage(Model model) {
		model.addAttribute("article", new Article());
		return "newArticle";
	}
	
}
