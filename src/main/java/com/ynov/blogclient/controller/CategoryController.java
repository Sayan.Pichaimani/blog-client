package com.ynov.blogclient.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ynov.blogclient.model.Article;
import com.ynov.blogclient.model.Category;
import com.ynov.blogclient.service.CategoryService;

@Controller
@RequestMapping("public")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/categories")
	public String categoriesPage(Model model, HttpSession session) {
		List<Category> categories = categoryService.getCategories();
		model.addAttribute("categories", categories);
		return "categories";
	}
	
	@GetMapping("/category/{id}")
	public ModelAndView categoryPage(@PathVariable(name = "id") Integer id, Model model) {
		Category category = categoryService.getCategoryById(id);
		model.addAttribute("category", category);
		//return "category";
		return new ModelAndView("redirect:/public/articles");
	}
}
