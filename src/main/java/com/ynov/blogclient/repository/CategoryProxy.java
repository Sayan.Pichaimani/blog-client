package com.ynov.blogclient.repository;

import java.nio.charset.Charset;
import java.util.List;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.ynov.blogclient.ApiProperties;
import com.ynov.blogclient.TokenContext;
import com.ynov.blogclient.model.Article;
import com.ynov.blogclient.model.Category;

@Component
public class CategoryProxy {

	@Autowired
	private ApiProperties props;
	
	@Autowired
	private TokenContext tokenContext;
	
	// Création d'un header pour la méthode Basic Auth 
		@SuppressWarnings("unused")
		private HttpHeaders createBasicAuthHeaders(String username, String password){
			return new HttpHeaders() {
				private static final long serialVersionUID = 1L;
				{
					String auth = username + ":" + password;
			        byte[] encodedAuth = Base64.encodeBase64( 
			            auth.getBytes(Charset.forName("US-ASCII")) );
			        String authHeader = "Basic " + new String( encodedAuth );
			        set( "Authorization", authHeader );
			    }
			};
		}
	
		// Création d'un header pour la méthode Bearer Token
		private HttpHeaders createTokenHeaders() {
			return new HttpHeaders() {
				private static final long serialVersionUID = 1L;
				{
					// utilisation du token qui est dans la classe ApiProperties
					String authHeader = "Bearer " + tokenContext.getToken();
					set("Authorization", authHeader);
					System.out.println("Provided token is : " + authHeader);
				}
			};
		}
	
	public List<Category> getCategories() {
		
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<List<Category>> response =
				restTemplate.exchange(
						props.getUrl() + "/categories", 
						HttpMethod.GET, 
						new HttpEntity<>(createTokenHeaders()), 
						new ParameterizedTypeReference<List<Category>>() {}
					);
		return response.getBody();
	}

	public Category getCategoryById(Integer id) {
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<Category> response =
				restTemplate.exchange(
						props.getUrl() + "/category/" + id, 
						HttpMethod.GET, 
						new HttpEntity<>(createTokenHeaders()), 
						Category.class);
		
		return response.getBody();
	}
	
	public void save(Category category) {
		RestTemplate restTemplate = new RestTemplate();
		
		HttpEntity<Category> request = new HttpEntity<Category>(category, createTokenHeaders());
		
		restTemplate.exchange(
				props.getUrl() + "/category",
				HttpMethod.POST,
				request,
				Category.class				
				);
	}	
	
}
